var text = "Je suis le texte a afficher avec une fonction";
var array = ["Apple", "Banana", "Orange"];


funcDisplayTable(text, array);
funcIf();
funcFor();
funcForArray();


function funcDisplayTable(text, array) {
  document.write(text);

  document.write('<table width="60%" cellpadding="5" border="1">');
  document.write('<tr>');

  for (let index = 0; index < array.length; index++) {
    document.write(`<td>${array[i]}</td>`);
  }

  document.write('</tr>');
  document.write('</table>');
}

function funcIf() {
  var int = 15;
  alert("La valeur de funcIf est " + int);

  if (int < 10) {
    alert("La valeur est plus petit que 10");
  } else {
    alert("La valeur est plus grand que 10");
  }
}


function funcFor() {
  for (let i = 0; i <= 10; i++) {
    document.write('</br>' + i + '</br>');
  }
}

function funcForArray() {
  document.write('<table width="60%" cellpadding="5" border="1">');

  for (let i = 0; i <= 10; i++) {

    document.write('<tr>');

    for (let j = 0; j <= 10; j++) {
      document.write(`<td>${i}, ${j}</td>`);
    }

    document.write('</tr>');
  }

  document.write('</table>');
}
