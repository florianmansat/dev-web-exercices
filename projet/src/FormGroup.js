import React, { Component } from 'react';
import './css/bootstrap.min.css';

class FormGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departements: [],
      administrations : [
        {
          code : 'bsn', desc : 'Bureau ou centre du service national'
        },
        {
          code : 'caa', desc : 'Cour administrative d’appel'
        },        
        {
          code : 'caf', desc : 'Caisse d’allocations familiales (CAF)'
        },        
        {
          code : 'centre_impots_fonciers', desc : 'Centre des impôts foncier et cadastre'
        },        
        {
          code : 'centre_penitentiaire', desc : 'Centre pénitentiaire'
        },        
        {
          code : 'centre_social', desc : 'Centre social'
        },        
        {
          code : 'crous', desc : 'CROUS et ses antennes'
        },        
        {
          code : 'mairie', desc : 'Mairie'
        }
      ],
      selectDepart: null,
      selectAdmin: null,
    };

    this.handleChangeDepart = this.handleChangeDepart.bind(this);
    this.handleChangeAdmin = this.handleChangeAdmin.bind(this);
  }

  handleChangeDepart(event) { this.setState({selectDepart: event.target.value}); }

  handleChangeAdmin(event) { this.setState({selectAdmin: event.target.value}); }

  componentDidMount() {
    fetch("https://geo.api.gouv.fr/departements")
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          departements: result
        });
      }
    );
  }

  render() {
    return (
      <form>
        <div className="form-row pb-5" style={{paddingLeft: 20 + 'px'}}>

          <div className="form-group col-md-3">
            <label htmlFor="selectDepartement">Département : </label>
            <select id="selectDepartement" className="form-control" onChange={this.handleChangeDepart}>
              <option defaultValue value="default" key="defaultDep">---------</option>
              {
                this.state.departements.map((dep, index) => {
                  return (
                    <option value={dep.code} key={index}>{dep.nom}</option>
                  )
                })
              }
            </select>
          </div>

          <div className="form-group col-md-4">
            <label htmlFor="selectAdministration">Administration : </label>
            <select id="selectAdministration" className="form-control" onChange={this.handleChangeAdmin}>
              <option defaultValue value="default" key="default">---------</option>
              {
                this.state.administrations.map((admi, index) => {
                  return (
                    <option value={admi.code} key={index}>{admi.desc}</option>
                  )
                })
              }
            </select>
          </div>

          <div className="form-group col-md-3" style={{paddingTop: 32 + 'px'}}>
          <button type="button" className="btn btn-primary" onClick={(evt) => 
            this.props.submit({ depart : this.state.selectDepart, admin : this.state.selectAdmin })}
          >Recherche une administration</button>
          </div>
          
          <div className="form-group col-md-1" style={{paddingTop: 32 + 'px'}}>
            <button type="button" className="btn btn-primary" onClick={this.props.reset}>Vider la recherche</button>
          </div>
          

        </div>
      </form>
    );
  }
}


export default FormGroup;
