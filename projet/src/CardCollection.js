import React, { Component } from 'react';
import './css/bootstrap.min.css';

class CardCollection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departements : []
    }
  }


  render() {
    
    if (this.props.data != null) {
      if (this.props.data.features.length > 0) {
        return (
          <div className="card-columns">
            {
              this.props.data.features.map((card, index) => {
                return (
                  <div className="card" key={index}>
                    <div className="card-body">
                      <h5 className="card-title">{card.properties.nom}</h5>
                      <p className="card-text text-muted">Téléphone : {card.properties.telephone}</p>
                    </div>
                  </div>
                )
              })
            }
          </div>
        );
      }
      else {
        return (<h2 className="text-center">Pas de données</h2>);
      }
    }
    else {
      return ("");
    }

  }
}


export default CardCollection;
