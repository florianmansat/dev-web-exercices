import React, { Component } from 'react';
import CardCollection from './CardCollection';
import FormGroup from './FormGroup';
import './css/bootstrap.min.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.updateSubmit = this.updateSubmit.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.state = {
      submitValue : null,
      data : null
    }  
  }

  updateSubmit(evt) {
    
    this.setState({
      submitValue : evt
    });

    fetch("http://etablissements-publics.api.gouv.fr/v3/departements/"+evt.depart+"/"+evt.admin)
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          data : result
        })
      }
    );
    
  }

  resetFilter() {
    this.setState({
      data : null
    });
  }

  render() {
    return (
      <div className="container p-3">
        <h1 className="text-center">Trouver son administration dans un département</h1>
        <hr className="pb-4"></hr>
        <FormGroup submit={this.updateSubmit} reset={this.resetFilter} />
        <CardCollection filter={this.state.submitValue} data={this.state.data} />
      </div>
    );
  }
}

export default App;
